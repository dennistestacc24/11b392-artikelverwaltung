//Ein Element unsichtbar machen
function hide(hideElement) {
	var element = document.getElementById(hideElement);
	element.classList.add("hidden");
}

//Ein Element wieder anzeigen 
function show(hideElement) {
	var element = document.getElementById(hideElement);
	element.classList.remove("hidden");
}

//Verbinden Button einfärben und Text ändern 
function makeConnectButtonRed(tElement) {
	var element = document.getElementById(tElement);
	element.classList.remove("btn-success");
	element.classList.add("btn-warning");
	element.innerText = "Disconnect from Server";
	var bscomponent = document.getElementById("bs-component");
	bscomponent.classList.add("height100");
}

//Verbinden Button einfärben und Text ändern 
function makeConnectButtonGreen(Element) {
	var element = document.getElementById(tElement);
	element.classList.remove("btn-warning");
	element.classList.add("btn-success");
	element.innerText = "Connect to SQL Server";
}

//Index weiterleitung Funktion 
function indexref() {
	location.href='index.php';
}

//Verbunden als Parameter übergeben 
function connectedref() {
	location.href='index.php?connect=true';
}

$(document).ready(function(){
$('.rowValue').on('focusout', (e) => {

    var element = document.getElementById("connected");
	element.innerHTML = '<h4 class="alert-heading">Saving Values....</h4><p class="mb-0"> Trying to save Values to SQL....<a class="alert-link"></a></p>';
	element.classList.remove("alert-primary");
    element.classList.add("alert-warning");
    window.location.href = 'index.php?connect=true&updatevalues=true';
});

$('.rowValue').on('input', (e) => {
  var element = document.getElementById("connected");
  element.innerHTML = '<h4 class="alert-heading">Pending Values!</h4><p class="mb-0"> Exit out of the Field to save Values: ' +e.currentTarget.innerHTML + '      \n       <a class="alert-link"></a></p>';
  element.classList.remove("alert-success");
  element.classList.add("alert-primary");
});
});