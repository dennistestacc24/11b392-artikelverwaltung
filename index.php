<!doctype html>

<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Artikelverwaltung</title>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="script.js"></script>
  <meta name="description" content="Softwarelösung Artikelverwaltung">
  <meta name="author" content="Dennis Grin und Maurice Gajic">
  <link rel="stylesheet" href="css/bootstrap.css?v=1.0">
  <link rel="stylesheet" href="css/design.css?v=1.0">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono&display=swap" rel="stylesheet">
</head>

<body >
	<div class="main">
		<div id="bs-component" class="bs-component">

				<?php 
					if(!(isset($_GET['connect']))) {
						echo '<div id="disconnected" class="alert alert-dismissible alert-danger">
						<button type="button" onclick="hide(`connected`)" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">You are not connected</h4>
						<p class="mb-0"> Please click Connect to connect to a Server<a class="alert-link"></a>.</p>
						</div>';
					}
				?>
				
				<?php 
					if(!(isset($_GET['updatevalues']))) {
						echo '<div id="connected" class="alert alert-dismissible alert-success hidden">
						<button type="button" onclick="hide(`connected`)" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">You are connected to the SQL Server</h4>
						<p class="mb-0"> You can now edit the table<a class="alert-link"></a>.</p>
						</div> '; 
					}
					else {
						echo '<div id="connected" class="alert alert-dismissible alert-success hidden">
						<button type="button" onclick="hide(`connected`)" class="close" data-dismiss="alert">×</button>
						<h4 class="alert-heading">Values updated!</h4>
						<p class="mb-0"> Values have been updated! <a class="alert-link"></a></p>
						</div> ';  
					}
				?>

				<?php 
					//Neuen Datensatz hinzufügen
					if((isset($_GET['addRow']))) {
						$conn=mysqli_connect("localhost","root","","awdb");
					
						//Fehlgeschlagene Verbindung Abfangen 
						if ($conn->connect_error) {
							die("Connection failed: " . $conn->connect_error);
						}

						$sql = "INSERT INTO awtabelle (Name, Beschreibung, Anzahl, Ort) VALUES ('-', '-', '-', '-')";
						
						//Falls Neuer Datensatz nicht erstellt werden konnte Fehlermeldung ausgeben
						if ($conn->query($sql) === TRUE) {
							echo "New record created successfully";
						} else {
							echo "Error: " . $sql . "<br>" . $conn->error;
						}

						$conn->close();
					}
				?>
				
				<div id="tableWrapper">
					<?php
						if (isset($_GET['connect'])) {		 
							require_once('sql.php'); 
							$connected = login();
						}
					?> 
				</div>
				
				<div class="buttonstop">
					<?php 
						if(isset($connected)) {
							echo '<button id="connectbutton" type="button" onclick="indexref()" href="index.php?connect=true" class="btn btn-success">Connect to SQL</button>';
							echo '<input style="width: 100px; display: inline-block; float:left; background-color: #ebebeb; margin-right:10px; border-radius: 5px;" type="text" readonly="" class="form-control-plaintext" id="seacrhbox" value="Suchen...">';
							echo '<button id="searchbutton" style="float:left" type="button" onclick="indexref()" href="index.php?search=true" class="btn btn-success">Suchen</button>';
							echo '<form id="addRowForm" action="index.php?connect=true&addRow=true" method="post"><button id="addrowbutton" type="submit" onclick="indexref()" href="index.php?connect=true" class="btn btn-primary">Add Row +</button></form>';
						} else {
							echo '<button id="connectbutton" type="button" onclick="connectedref()" href="index.php?connect=true" class="btn btn-success">Connect to SQL</button>';	
						}
					?>
				</div>
				
				<?php
					if(isset($connected)) {
							if($connected=="is") {
								echo '<script type="text/javascript">makeConnectButtonRed("connectbutton");</script>';
							} else {
								echo '<script type="text/javascript">makeConnectButtonGreen("connectbutton");</script>';
							}
						}
				?>
			</div>
	</body>
</html>